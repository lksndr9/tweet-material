import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { UserModel } from '../../models/user.model';
import { TweetStorageInterface } from '../../interfaces/tweet-storage.interface';
import { MatSnackBar } from '@angular/material/snack-bar';
import { LoginWrapComponent } from '../login-wrap/login-wrap.component';

@Component({
  selector: 'app-register-material',
  templateUrl: './register-material.component.html',
  styleUrls: ['./register-material.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class RegisterMaterialComponent implements OnInit {

  // Form input group
  inputFormGroup: FormGroup = new FormGroup ({
    firstName: new FormControl(''),
    lastName: new FormControl (''),
    username: new FormControl (''),
    email: new FormControl (''),
    phone: new FormControl (''),
    password: new FormControl ('')
  })
  
  // Declare variables
  allUsers: UserModel[] = [];
  hide: boolean = true;
  usersKey: string = 'users-tweet';
  alreadyExist: boolean = true;
  
  constructor(
    private formBuilder: FormBuilder,
    private userStorageInterface: TweetStorageInterface,
    private snackBar: MatSnackBar,
    private loginwrap: LoginWrapComponent
    )
    {}

    ngOnInit(): void {
      // Get all users from storage
      this.allUsers = this.userStorageInterface.getUserDataStorage(this.usersKey);
      // Initiate input form 
      this.inputFormGroup = this.formBuilder.group(
      {
        firstName: ['', {
          validators: [Validators.required, Validators.minLength(1)],
          updateOn: 'blur'
        }],
        lastName: ['', {
          validators: [Validators.required, Validators.minLength(1)],
          updateOn: 'blur'
        }],
        username: ['', {
        validators: [Validators.required, Validators.minLength(3), Validators.maxLength(10)],
        updateOn: 'blur'
        }],
        email: ['', {
          validators: [Validators.required, Validators.email],
          updateOn: 'blur'
        }],
        phone: ['', {
          validators: [Validators.required, Validators.minLength(9)],
          updateOn: 'blur'
        }],
        password: ['', {
          validators: [Validators.required, Validators.minLength(4)],
          updateOn: 'blur'
        }]
      })
    }

  // Getters for input form
  get firstName(): FormControl {return this.inputFormGroup.get('firstName') as FormControl}
  get lastName(): FormControl {return this.inputFormGroup.get('lastName') as FormControl}
  get username(): FormControl {return this.inputFormGroup.get('username') as FormControl}
  get email(): FormControl {return this.inputFormGroup.get('email') as FormControl}
  get phone(): FormControl {return this.inputFormGroup.get('phone') as FormControl}
  get password(): FormControl {return this.inputFormGroup.get('password') as FormControl}
  
  // Set invalid form cases
  firstNameInvalid(): boolean {return this.firstName.invalid && (this.firstName.dirty || this.firstName.touched)}
  lastNameInvalid(): boolean {return this.lastName.invalid && (this.lastName.dirty || this.lastName.touched)}
  usernameInvalid(): boolean {return this.username.invalid && (this.username.dirty || this.username.touched)}
  emailInvalid(): boolean {return this.email.invalid && (this.email.dirty || this.email.touched) && this.alreadyExist}
  phoneInvalid(): boolean {return this.phone.invalid && (this.phone.dirty || this.phone.touched)}
  passwordInvalid(): boolean {return this.password.invalid && (this.password.dirty || this.password.touched)}

  onSubmit() {

    // Chek if email already exist
    this.alreadyExist = false;
    this.checkUserExist(this.email.value);
    if (this.alreadyExist) {return}

    // Mark all fields of form as dirty
    this.firstName.markAsDirty();
    this.lastName.markAsDirty();
    this.username.markAsDirty();
    this.email.markAsDirty();
    this.phone.markAsDirty();

    // If anything is invalid in form, do nothing and return to register
    if (this.inputFormGroup.invalid) return;

    // Prepare object for new input
    const newUser: UserModel = {
      email: this.email.value,
      password: this.password.value,
      username: this.username.value,
      firstName: this.firstName.value,
      lastName: this.lastName.value,
      phone: '+' + this.phone.value,
      createdAt: new Date
    }

    // If no users exist, let it be empty array
    if (this.allUsers === null) {this.allUsers = []}
    
    // Add new user to array 
    this.allUsers.push(newUser);
    
    // Save array to storage
    this.userStorageInterface.saveUserDataStorage(this.usersKey,this.allUsers);
    
    // Display confirmation
    this.showMessage('Registration successful, please login.');
    
    // Return to login
    this.loginwrap.rotateLogin();
  }

  // Show message with SnackBar
  showMessage(message: string) {
    return this.snackBar.open(message, '', {
      horizontalPosition: 'right',
      verticalPosition: 'bottom',
      duration: 2000})
  }

  // Check if user already exist
  checkUserExist(email: string): boolean {
    if (this.allUsers === null) return false;
    let foundedUser = this.allUsers.find( user => user.email === email);
      // if not founded it is undefined. Put false flag to 'userOk' that it is not founded
      if (foundedUser === undefined) { this.alreadyExist = false; return false } 
      // if founded, put flag 
      else { this.alreadyExist = true; return true }
  }
}
