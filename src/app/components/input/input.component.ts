import { Component, OnInit } from '@angular/core';
import { UserModel } from '../../models/user.model';
import { MatDialog} from '@angular/material/dialog';
import { Router } from '@angular/router';
import { UserStoreInterface } from '../../interfaces/user-store.interface';
import { ConfirmDialogComponent, ConfirmDialogModel } from '../confirmation/confirmation.component';
import { UserStoreService } from 'src/app/services/user-store.service';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})

export class InputComponent implements OnInit  {

  constructor(
    public dialog: MatDialog,
    private userStoreInterface: UserStoreInterface,
    private userStoreService: UserStoreService,
    private router: Router
    ) {}
    
  ngOnInit(): void {}

  onSubmit() {}

  // Clean current user data and send to login page
  logOut() {
    this.userStoreService.currentUser = {} as UserModel;
    this.userStoreInterface.userDelete();
    this.router.navigate(['login']);
  }

  confirmLogOut(): void {
    // Message for universal confirmation
    const message = `Are you sure you want to log out?`;
    // Prepare dialog
    const dialogData = new ConfirmDialogModel("Confirm Log out", message);
    // Open dialog
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: "400px",
      data: dialogData
    });
    // After dialog closed, do what is chosen
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (dialogResult === true) { this.logOut() };
    });
  }
}