import { TestBed } from '@angular/core/testing';

import { TweetStorageService } from './tweet-storage.service';

describe('TweetStorageService', () => {
  let service: TweetStorageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TweetStorageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
