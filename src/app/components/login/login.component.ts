import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { UserModel } from '../../models/user.model';
import { TweetStorageInterface } from '../../interfaces/tweet-storage.interface';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UserStoreInterface } from '../../interfaces/user-store.interface';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  // Form input group
  inputFormGroup: FormGroup = new FormGroup ({
  email: new FormControl (''),
  password: new FormControl ('')
  })
  
  // Declare variables
  hide: boolean = true;
  allUser: UserModel[] = [];
  key: string = 'users-tweet';
  userOk: boolean = true;
  passwordOk: boolean = true;

  constructor(
    private formBuilder: FormBuilder,
    private storageInterface: TweetStorageInterface,
    private userStoreInterface: UserStoreInterface,
    private router: Router,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
  
    // Get existing users 
    this.allUser = this.storageInterface.getUserDataStorage(this.key);
  
    // Initiate input form 
    this.inputFormGroup = this.formBuilder.group(
      {
        email: ['', {
          validators: [Validators.required, Validators.email],
          updateOn: 'blur'
        }],
        password: ['', {
          validators: [Validators.required, Validators.minLength(4)],
          updateOn: 'blur'
        }]
      })
  }
  // Form getters and invalid setting
  get email(): FormControl {return this.inputFormGroup.get('email') as FormControl}
  get password(): FormControl {return this.inputFormGroup.get('password') as FormControl}

  emailInvalid(): boolean {return this.email.invalid && (this.email.dirty || this.email.touched)}
  passwordInvalid(): boolean {return this.password.invalid && (this.password.dirty || this.password.touched)}

  onSubmit() {

    this.userOk = true;
    this.passwordOk = true;
    this.email.markAsDirty();
    this.password.markAsDirty();

    if (this.inputFormGroup.invalid)  return 

    // Check if user list exist and if not, declare as empty array
    if (this.allUser === null) this.allUser = [];
  
    // Find typed user email 
    let foundedUser = this.allUser.find( user => user.email === this.email.value);
  
    // If not founded it is undefined. Put false flag to 'userOk' that it is not founded
    if (foundedUser === undefined) {this.userOk = false; return} 
      else {
        
        // If founded, put flag and check password 
        this.userOk = true;
        if (foundedUser.password === this.password.value) {this.passwordOk = true} else {this.passwordOk = false}
        
        // If email and password are correct, proceed to input
        if (this.userOk && this.passwordOk) {
            this.userStoreInterface.userWrite(foundedUser); 
            // use material SnackBar for message
            this.openDialog('Login successful.')
            this.router.navigate(['input']);
        }
      }
  }
    
  // Show message to user
  openDialog(message: string) {
    return this.snackBar.open(message, '', {
      horizontalPosition: 'right',
      verticalPosition: 'bottom',
      duration: 2000})
  }
}

  

