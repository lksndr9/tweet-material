import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TweetDivComponent } from './tweet-div.component';

describe('TweetDivComponent', () => {
  let component: TweetDivComponent;
  let fixture: ComponentFixture<TweetDivComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TweetDivComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TweetDivComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
