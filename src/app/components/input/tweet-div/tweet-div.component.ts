import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { DatePipe } from '@angular/common';
import { ChangeDetectorRef, Component, NgZone, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatMenuTrigger } from '@angular/material/menu';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { DatabaseInterface } from 'src/app/interfaces/database.interface';
import { UserStoreInterface } from 'src/app/interfaces/user-store.interface';
import { TweetObjectModel } from 'src/app/models/tweet-object.model';
import { UserModel } from 'src/app/models/user.model';
import { ConfirmDialogComponent, ConfirmDialogModel } from '../../confirmation/confirmation.component';

@Component({
  selector: 'app-tweet-div',
  templateUrl: './tweet-div.component.html',
  styleUrls: ['./tweet-div.component.scss']
})
export class TweetDivComponent implements OnInit {

// Bind with DOM
@ViewChild('menuTrigger') menuTrigger!: MatMenuTrigger;
@ViewChild(MatPaginator) paginator!: MatPaginator;
@ViewChild('autosize') autosize!: CdkTextareaAutosize;

// Observable used for async holding of paged tweet
obs!: Observable<any>;

// Forming of formgroup for input
inputFormGroup: FormGroup = new FormGroup ({tweet: new FormControl ('')})

// Current user loged in
currentUser: UserModel = {} as UserModel;

// Pipe for date transform
pipe = new DatePipe('en-US');

// Paginator initial parameters
pageSize = 5;
currentPage = 0;
pageSizeOptions: number[] = [5, 10, 25, 100];
totalRows = this.dataBase.getPagedTweets(this.pageSize,this.currentPage).count;

// Reset tweets page
tweetsAll: TweetObjectModel[] = [];

// Setting datasource for paginator
dataSource: MatTableDataSource<TweetObjectModel> = new MatTableDataSource<TweetObjectModel>(this.tweetsAll);
  
constructor(
  public dialog: MatDialog,
  private changeDetectorRef: ChangeDetectorRef,
  private userStoreInterface: UserStoreInterface,
  private ngZone: NgZone,
  private router: Router,
  private dataBase: DatabaseInterface,
  ) {}
  
ngOnInit(): void {
  
  // initiate paginator
  this.dataSource.paginator = this.paginator;
  // get paged source of tweets
  this.tweetsAll = this.dataBase.getPagedTweets(this.pageSize,this.currentPage).tweetsPage;
  // render table and reconnect with new array
  this.refreshTable();
  this.changeDetectorRef.detectChanges();
  this.dataSource = new MatTableDataSource<TweetObjectModel>(this.tweetsAll);
  this.obs = this.dataSource.connect();
  this.currentUser = this.userStoreInterface.userGet();
  if (Object.keys(this.currentUser).length === 0) {this.router.navigate(['login']);}
}

get tweet(): FormControl {return this.inputFormGroup.get('tweet') as FormControl}
tweetInvalid(): boolean {return this.tweet.invalid && (this.tweet.dirty || this.tweet.touched)}

onSubmit() {
  
  //new tweet object writen by user to be stored in array
  const newTweet: TweetObjectModel = {
    username: this.currentUser.username,
    body: this.tweet.value,
    writenAt: new Date
  }
  
  // if message is empty, do nothing
  if (newTweet.body === '') {return}
  
  //if array is null, let it be empty array
  if (this.tweetsAll === null) { this.tweetsAll = []}
  
  //save in storage
  this.dataBase.saveTweet(newTweet);
  
  // make input field empty in case of multiple clicks...
  this.tweet.setValue('');

  // Return to first page of tweets
  this.currentPage = 0;
  
  //render table
  this.refreshTable();
  this.currentUser = this.userStoreInterface.userGet();
}

convertDate(date: Date): string | null {
  //transform date
  return this.pipe.transform(date, 'd MMMM, y, h:mm:ss a');
}

refreshTable() {
  //read all tweets from local storage
  let databaseResp = this.dataBase.getPagedTweets(this.pageSize,this.currentPage);

  //take page for display
  this.tweetsAll = databaseResp.tweetsPage;
  
  //get total number of tweets to paginator
  this.totalRows = databaseResp.count;
  
  //put array in MatDataSource object
  this.dataSource.data = this.tweetsAll;
  this.dataSource.connect().next(this.tweetsAll);
  
  //refresh paginator
  this.obs = this.dataSource.connect();
}

// detect changes in paginator
pageChanged(event: PageEvent) {
  this.pageSize = event.pageSize;
  this.currentPage = event.pageIndex;
  this.refreshTable();
}

// delete tweet from array (must be yours)
deleteTweet(tweet: TweetObjectModel) {
  this.dataBase.deleteTweet(tweet);
  this.refreshTable();
}

// Clean current user data and send to login page
logOut() {
  this.currentUser = {} as UserModel;
  this.userStoreInterface.userDelete();
  this.router.navigate(['login']);
}

triggerResize() {
  // Wait for changes to be applied, then trigger textarea resize.
  this.ngZone.onStable.pipe(take(1)).subscribe(() => this.autosize.resizeToFitContent(true));
}

confirmDelete(tweet: TweetObjectModel): void {
  // Message for universal confirmation
  const message = `Are you sure you want to delete this message?`;
  // Prepare dialog
  const dialogData = new ConfirmDialogModel("Confirm Delete", message);
  // Open dialog
  const dialogRef = this.dialog.open(ConfirmDialogComponent, {
    maxWidth: "400px",
    data: dialogData
  });
  // After dialog closed, do what is chosen
  dialogRef.afterClosed().subscribe(dialogResult => {
    if (dialogResult === true) { this.deleteTweet(tweet)};
  });
}

confirmLogOut(): void {
  // Message for universal confirmation
  const message = `Are you sure you want to log out?`;
  // Prepare dialog
  const dialogData = new ConfirmDialogModel("Confirm Log out", message);
  // Open dialog
  const dialogRef = this.dialog.open(ConfirmDialogComponent, {
    maxWidth: "400px",
    data: dialogData
  });
  // After dialog closed, do what is chosen
  dialogRef.afterClosed().subscribe(dialogResult => {
    if (dialogResult === true) { this.logOut() };
  });
}
}