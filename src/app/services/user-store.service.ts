import { Injectable } from '@angular/core';
import { UserModel } from '../models/user.model';

//* Service for communication with backend about users data  *//

@Injectable({
  providedIn: 'root'
})
export class UserStoreService {

// Current user used for interaction between components
currentUser = {} as UserModel;

constructor() { }

  // Receive current user
  userWrite (userData: UserModel) {
    this.currentUser = userData;
  }

  // Return current user 
  userGet () :UserModel {
    return this.currentUser;
  }

  // Delete current user
  userDelete(): void {
    this.currentUser = {} as UserModel
  }
}
