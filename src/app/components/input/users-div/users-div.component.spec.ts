import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersDivComponent } from './users-div.component';

describe('UsersDivComponent', () => {
  let component: UsersDivComponent;
  let fixture: ComponentFixture<UsersDivComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UsersDivComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersDivComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
