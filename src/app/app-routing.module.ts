import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InputComponent } from './components/input/input.component';
import { LoginWrapComponent } from './components/login-wrap/login-wrap.component';

const routes: Routes = [
  { path: '', component: LoginWrapComponent},
  { path: 'input', component: InputComponent},
  { path: 'login', component: LoginWrapComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
