import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InputComponent } from './components/input/input.component';
import { LoginComponent } from './components/login/login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RegisterMaterialComponent } from './components/register-material/register-material.component';
import { ErrorStateMatcher, ShowOnDirtyErrorStateMatcher } from '@angular/material/core';
import { TweetStorageService } from './services/tweet-storage.service';
import { FlexLayoutModule } from '@angular/flex-layout';
import { LoginWrapComponent } from './components/login-wrap/login-wrap.component';
import { TweetStorageInterface } from './interfaces/tweet-storage.interface';
import { DatabaseInterface } from './interfaces/database.interface';
import { DatabaseService } from './services/database.service';
import { UserStoreInterface } from './interfaces/user-store.interface';
import { UserStoreService } from './services/user-store.service';
import { MaterialModule } from '../MaterialModule';
import { ConfirmDialogComponent } from './components/confirmation/confirmation.component';
import { TweetDivComponent } from './components/input/tweet-div/tweet-div.component';
import { UsersDivComponent } from './components/input/users-div/users-div.component';


@NgModule({
  declarations: [
    AppComponent,
    InputComponent,
    LoginComponent,
    RegisterMaterialComponent,
    LoginWrapComponent,
    ConfirmDialogComponent,
    TweetDivComponent,
    UsersDivComponent
    
  ],
  imports: [
    MaterialModule,
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
  ],
  providers: [
    {provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher},
    {provide: TweetStorageInterface, useClass: TweetStorageService},
    {provide: DatabaseInterface, useClass: DatabaseService},
    {provide: UserStoreInterface, useClass: UserStoreService}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
