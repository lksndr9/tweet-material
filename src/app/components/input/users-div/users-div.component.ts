import { Component, OnInit } from '@angular/core';
import { UserStoreInterface } from '../../../interfaces/user-store.interface';
import { UserStoreService } from 'src/app/services/user-store.service';
import { UserModel } from 'src/app/models/user.model';
import { DatabaseInterface } from '../../../interfaces/database.interface';

@Component({
  selector: 'app-users-div',
  templateUrl: './users-div.component.html',
  styleUrls: ['./users-div.component.scss']
})
export class UsersDivComponent implements OnInit {

  // Current user loged in
  currentUser = this.userStoreService.currentUser; 

  // All users
  allUser: UserModel[] = [];
  
  // Active users in this tweet
  activeUsers: string[] = []; 

  constructor(
    private userStoreInterface: UserStoreInterface,
    private userStoreService: UserStoreService,
    private database: DatabaseInterface
    ) { }

  ngOnInit(): void {
    this.currentUser = this.userStoreInterface.userGet();
    this.activeUsers = this.database.getActiveUsers();
  }

 }
